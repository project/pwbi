(function (Drupal) {
  Drupal.behaviors.pwbi_embed = {
    attach: function (context, settings) {
      Object.entries(settings.pwbi_embed).forEach((entry) => {
        const [key, embedSettings] = entry;
        const embedContainer = document.getElementById(embedSettings.id);
        // powerbi is the global object of the powerbi-client library.
        for (const i in powerbi.embeds) {
          if (powerbi.embeds[i].element.id === embedSettings.id) {
            return;
          }
        }
        const embedConfiguration = embedSettings;
        embedConfiguration.settings.localeSettings = {
          language: drupalSettings.path.currentLanguage,
        };
        // Get a reference to the HTML element that contains the embedded report.
        const powerBiEmbedParams = {
          embedConfiguration: embedConfiguration,
          embedContainer: embedContainer,
        };
        // Embed the visual.
        const PowerBiPreEmbed = new CustomEvent('PowerBiPreEmbed', {
          detail: powerBiEmbedParams,
        });
        window.dispatchEvent(PowerBiPreEmbed);
        const report = powerbi.embed(
          powerBiEmbedParams.embedContainer,
          powerBiEmbedParams.embedConfiguration
        );
        // Embed the visual.
        const PowerBiPostEmbed = new CustomEvent('PowerBiPostEmbed', {
          detail: report,
        });
        report.on('loaded', function () {
          window.dispatchEvent(PowerBiPostEmbed);
        });
      });
    },
    detach: function () {},
  };
})(Drupal);
