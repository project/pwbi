((Drupal) => {
  let flag = false;
  let pwbiBannerCookie= {
    cookieName: 'Drupal.pwbi_banner.accepted_disclaimer',
    bannerSelector: '.pwbi-disclaimer-banner',
    bannerAction: ".pwbi-disclaimer-banner .pwbi-disclaimer-accept button",
    domain: '',
    expires: 7,
    cookieLibraryExists: function() {
      return "get" in Cookies && "set" in Cookies;
    },
    getCookie: function() {
      return  Cookies.get(this.cookieName);
    },
    acceptDisclaimer: function() {
      Cookies.set(this.cookieName, "true", {
        expires: this.expires,
        domain: this.domain,
      })
      this.hideBanners();
    },
    isDisclaimerAccepted: function() {
      return this.getCookie() === "true";
    },
    hideBanners: function() {
      document.querySelectorAll(this.bannerSelector).forEach((banner) => {
        banner.style.display = 'none';
      });
    },
    showBanners: function() {
      document.querySelectorAll(this.bannerSelector).forEach((banner) => {
        banner.style.display = 'display';
      });
    }
  }
  Drupal.behaviors.pwbi_banner = {
    attach(context, settings) {
      if (flag === false) {
        flag = true;
        for (let i = 0; i < settings.pwbi_banner.length; i++) {
          if (settings.pwbi_banner[i])
            pwbiBannerCookie[i] = settings.pwbi_banner[i];
        }
        pwbiBannerCookie.getCookie()
        if (pwbiBannerCookie.cookieLibraryExists() === false || pwbiBannerCookie.isDisclaimerAccepted() === true) {
          pwbiBannerCookie.hideBanners();
          return;
        }
        pwbiBannerCookie.showBanners();
        document.querySelectorAll(pwbiBannerCookie.bannerAction).forEach((acceptButton) => {
          acceptButton.addEventListener("click", () => {
            pwbiBannerCookie.acceptDisclaimer();
          });
        });
      }
    }
  }
})(Drupal);
